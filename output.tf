## private key

output "algorithm" {
  value = var.algorithm
}

output "ecdsa_curve" {
  value = var.ecdsa_curve
}

output "rsa_bits" {
  value = var.rsa_bits
}

output "private_key_pem" {
  sensitive = true
  value     = data.tls_public_key.this.private_key_pem
}

output "public_key_pem" {
  sensitive = true
  value     = data.tls_public_key.this.public_key_pem
}

output "public_key_openssh" {
  sensitive = true
  value     = data.tls_public_key.this.public_key_openssh
}

output "private_key_pem_path" {
  value = var.output_files ? local_file.private_key_pem[0].filename : ""
}

output "public_key_pem_path" {
  value = var.output_files ? local_file.public_key_pem[0].filename : ""
}

output "public_key_openssh_path" {
  value = var.output_files ? local_file.public_key_openssh[0].filename : ""
}

output "public_key_fingerprint_md5" {
  value = data.tls_public_key.this.public_key_fingerprint_md5
}

## self signed certificate
output "cert_pem" {
  sensitive = true
  value     = tls_self_signed_cert.this.cert_pem
}

output "cert_path" {
  value = var.output_files ? local_file.cert[0].filename : ""
}

output "subject" {
  value = tls_self_signed_cert.this.subject[0]
}

output "allowed_uses" {
  value = tls_self_signed_cert.this.allowed_uses
}

output "dns_names" {
  value = tls_self_signed_cert.this.dns_names
}

output "ip_addresses" {
  value = tls_self_signed_cert.this.ip_addresses
}

output "uris" {
  value = tls_self_signed_cert.this.uris
}

output "early_renewal_hours" {
  value = tls_self_signed_cert.this.early_renewal_hours
}

output "is_ca" {
  value = tls_self_signed_cert.this.is_ca_certificate
}

output "validity_period_hours" {
  value = tls_self_signed_cert.this.validity_period_hours
}

output "validity_start_time" {
  value = tls_self_signed_cert.this.validity_start_time
}

output "validity_end_time" {
  value = tls_self_signed_cert.this.validity_end_time
}

# All informations
output "cert_infos" {
  value = {
    algorithm                  = var.algorithm
    ecdsa_curve                = var.ecdsa_curve
    rsa_bits                   = var.rsa_bits
    public_key_fingerprint_md5 = data.tls_public_key.this.public_key_fingerprint_md5
    subject                    = tls_self_signed_cert.this.subject[0]
    allowed_uses               = tls_self_signed_cert.this.allowed_uses
    dns_names                  = tls_self_signed_cert.this.dns_names
    ip_addresses               = tls_self_signed_cert.this.ip_addresses
    uris                       = tls_self_signed_cert.this.ip_addresses
    is_ca                      = tls_self_signed_cert.this.is_ca_certificate
    early_renewal_hours        = tls_self_signed_cert.this.early_renewal_hours
    validity_period_hours      = tls_self_signed_cert.this.validity_period_hours
    validity_start_time        = tls_self_signed_cert.this.validity_start_time
    validity_end_time          = tls_self_signed_cert.this.validity_end_time
  }
}

output "cert_contents" {
  sensitive = true
  value = {
    private_key_pem            = data.tls_public_key.this.private_key_pem
    public_key_pem             = data.tls_public_key.this.public_key_pem
    public_key_openssh         = data.tls_public_key.this.public_key_openssh
    public_key_fingerprint_md5 = data.tls_public_key.this.public_key_fingerprint_md5
    cert_pem                   = tls_self_signed_cert.this.cert_pem
  }
}

output "cert_paths" {
  value = {
    private_key_pem_path    = var.output_files ? local_file.private_key_pem[0].filename : ""
    public_key_pem_path     = var.output_files ? local_file.public_key_pem[0].filename : ""
    public_key_openssh_path = var.output_files ? local_file.public_key_openssh[0].filename : ""
    cert_path               = var.output_files ? local_file.cert[0].filename : ""
  }
}
