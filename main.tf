## Private key

resource "tls_private_key" "this" {
  count       = var.private_key_pem == "" ? 1 : 0
  algorithm   = var.algorithm
  ecdsa_curve = var.ecdsa_curve
  rsa_bits    = var.rsa_bits
}

data "tls_public_key" "this" {
  private_key_pem = var.private_key_pem == "" ? tls_private_key.this[0].private_key_pem : var.private_key_pem
}

resource "local_file" "private_key_pem" {
  count                = var.output_files ? 1 : 0
  filename             = "${var.output_path}/${var.output_name}_key.pem"
  file_permission      = "0600"
  directory_permission = "0700"
  sensitive_content    = chomp(data.tls_public_key.this.private_key_pem)
}

resource "local_file" "public_key_pem" {
  count                = var.output_files ? 1 : 0
  filename             = "${var.output_path}/${var.output_name}_public.pem"
  file_permission      = "0600"
  directory_permission = "0700"
  sensitive_content    = chomp(data.tls_public_key.this.public_key_pem)
}

resource "local_file" "public_key_openssh" {
  count                = var.output_files ? 1 : 0
  filename             = "${var.output_path}/${var.output_name}_openssh.public"
  file_permission      = "0600"
  directory_permission = "0700"
  sensitive_content    = chomp(data.tls_public_key.this.public_key_openssh)
}

## Self signed certificate

resource "tls_self_signed_cert" "this" {
  key_algorithm         = var.algorithm
  private_key_pem       = var.private_key_pem == "" ? tls_private_key.this[0].private_key_pem : var.private_key_pem
  is_ca_certificate     = var.is_ca
  dns_names             = var.dns_names
  ip_addresses          = var.ip_addresses
  uris                  = var.uris
  early_renewal_hours   = var.early_renewal_hours
  set_subject_key_id    = var.set_subject_key_id
  validity_period_hours = var.validity_period_hours
  allowed_uses          = var.allowed_uses

  subject {
    common_name         = var.common_name
    organization        = var.organization
    organizational_unit = var.organizational_unit
    street_address      = var.street_addresses
    locality            = var.locality
    province            = var.province
    country             = var.country
    postal_code         = var.postal_code
    serial_number       = var.serial_number
  }
}

resource "local_file" "cert" {
  count                = var.output_files ? 1 : 0
  filename             = "${var.output_path}/${var.output_name}.crt"
  file_permission      = "0600"
  directory_permission = "0700"
  sensitive_content    = chomp(tls_self_signed_cert.this.cert_pem)
}
